public class Dog implements Animal{
    String dogName;

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public void voice()
    {
        System.out.println("Bark Bark "+dogName);
    }
}
